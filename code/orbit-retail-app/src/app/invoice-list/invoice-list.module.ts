import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { InvoiceListComponent } from './invoice-list.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {InvoiceDetailComponent} from './invoice-detail/invoice-detail.component';

const routes: Routes = [
  {
    path: '',
    component: InvoiceListComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    NgxDatatableModule
  ],
  declarations: [InvoiceListComponent, InvoiceDetailComponent]
})
export class InvoiceListModule {}