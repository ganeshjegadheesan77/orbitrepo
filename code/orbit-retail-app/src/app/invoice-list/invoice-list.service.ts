import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InvoiceListService {
  private _userIsAuthenticated = true;
  private _userId = 'xyz';
  public invoiceList : any;

  get userIsAuthenticated() {
    return this._userIsAuthenticated;
  }

  get userId() {
    return this._userId;
  }

  constructor(private httpClient:HttpClient) {}

  login(username, password) {
    this.authenticate(username, password);
    this._userIsAuthenticated = true;
  }

  logout() {
    this._userIsAuthenticated = false;
  }

  authenticate(username, password) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept' : 'application/json',
        'Content-Type':  'application/json'
        })
      };

    let postData = {
            "username": username,
            "password": password
    }

    this.httpClient.post("http://localhost:8240/OrionAPIServices/GenericResultBuilderService/buildResults", postData, httpOptions)
      .subscribe(data => {
        console.log(data);
       }, error => {
        console.log(error);
      });
  }  

  async getAllInvoices() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept' : 'application/json',
        'Content-Type':  'application/json'
        })
      };

    let postData = {
        "_keyword_": "invoice"
    }

    await this.httpClient.post("http://localhost:8240/OrionAPIServices/GenericResultBuilderService/buildResults", postData, httpOptions)
      .subscribe(data => {
        this.invoiceList = data["data"];
        let i = 1;
        this.invoiceList.forEach(element => {
            element.id = i;
            i++;
        });
        console.log(this.invoiceList);
       }, error => {
        console.log(error);
      });
  }  
}
