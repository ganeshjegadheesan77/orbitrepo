import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { InvoiceListService } from './invoice-list.service';

export interface Data {
  movies: string;
}

@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoiceListComponent implements OnInit {

  public data: Data;
  public columns: any;
  public rows: any;

  constructor(
    private http: HttpClient,
    private router: Router,
    private invoiceListService: InvoiceListService
  ) {
    this.columns = [
      { name: 'Id', prop:'id' },
      { name: 'InvoiceNo', prop: 'InvoiceNo' },
      { name: 'Date', prop: 'Date' },
      { name: 'TransactionCode', prop: 'TransactionCode' },
      { name: 'Customer', prop: 'Customer' }
    ];

    this.invoiceListService.getAllInvoices(); //.subscribe(data => {
      setTimeout(
        () => {
          console.log("Hi...", invoiceListService.invoiceList);
          this.rows = invoiceListService.invoiceList;
        }, 1500);
      
/*      
      }, error => {
      console.log(error);
    });
*/    
    
/*
    this.http.get<Data>('../../assets/movies.json')
      .subscribe((res) => {
        console.log(res)
        this.rows = res.movies;
      });
*/      
  }

  onActivate(event: any) {
    if(event.type == 'click') {
        console.log(event.row);
        console.log(event.row.id);
        if(event.row.id) {
          this.router.navigateByUrl('/invoice-detail');
        }
    }
  }

  ngOnInit() {
    console.log("InvoiceListComponent ngonInit");
  }

}
