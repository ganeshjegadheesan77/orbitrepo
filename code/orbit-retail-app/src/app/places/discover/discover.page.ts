import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { SegmentChangeEventDetail } from '@ionic/core';
import { Subscription } from 'rxjs';

import { PlacesService } from '../places.service';
import { Place } from '../place.model';
import { AuthService } from '../../auth/auth.service';

import { Chart } from "chart.js";

@Component({
  selector: 'app-discover',
  templateUrl: './discover.page.html',
  styleUrls: ['./discover.page.scss']
})
export class DiscoverPage implements OnInit, OnDestroy {
  @ViewChild("doughnutCanvas") doughnutCanvas: ElementRef;
  @ViewChild("bubbleCanvas") bubbleCanvas: ElementRef;
  @ViewChild("hBarCanvas") hBarCanvas: ElementRef;
  @ViewChild("gBarCanvas") gBarCanvas: ElementRef;

  private doughnutChart: Chart;
  private bubbleChart: Chart;
  private hBarChart: Chart;
  private gBarChart: Chart;
  
  loadedPlaces: Place[];
  listedLoadedPlaces: Place[];
  relevantPlaces: Place[];
  private placesSub: Subscription;

  constructor(
    private placesService: PlacesService,
    private menuCtrl: MenuController,
    private authService: AuthService
  ) {}

  ionViewDidEnter(){
    this.createDoughnutChart();
    this.createBubbleChart();
    this.createHorizontalBarChart();
    this.createGroupedBarChart();
  }

  ngOnInit() {
    this.placesSub = this.placesService.places.subscribe(places => {
      this.loadedPlaces = places;
      this.relevantPlaces = this.loadedPlaces;
      this.listedLoadedPlaces = this.relevantPlaces.slice(1);
    });
  }

  onOpenMenu() {
    this.menuCtrl.toggle();
  }

  onFilterUpdate(event: CustomEvent<SegmentChangeEventDetail>) {
    if (event.detail.value === 'all') {
      this.relevantPlaces = this.loadedPlaces;
      this.listedLoadedPlaces = this.relevantPlaces.slice(1);
    } else {
      this.relevantPlaces = this.loadedPlaces.filter(
        place => place.userId !== this.authService.userId
      );
      this.listedLoadedPlaces = this.relevantPlaces.slice(1);
    }
  }

  ngOnDestroy() {
    if (this.placesSub) {
      this.placesSub.unsubscribe();
    }
  }

  createBubbleChart(){
    this.bubbleChart = new Chart(this.bubbleCanvas.nativeElement,{
      type: 'bubble',
    data: {
      labels: "",
      datasets: [
        {
          label: ["ToorDal"],
          backgroundColor: "rgba(255,221,50,0.2)",
          borderColor: "rgba(255,221,50,1)",
          data: [{
            x: 100,
            y: 5000,
            r: 15
          }]
        }, {
          label: ["Rice"],
          backgroundColor: "rgba(60,186,159,0.2)",
          borderColor: "rgba(60,186,159,1)",
          data: [{
            x: 250,
            y: 15000,
            r: 10
          }]
        }, {
          label: ["Sugar"],
          backgroundColor: "rgba(0,0,0,0.2)",
          borderColor: "#000",
          data: [{
            x: 160,
            y: 20500,
            r: 15
          }]
        }, {
          label: ["Wheat"],
          backgroundColor: "rgba(193,46,12,0.2)",
          borderColor: "rgba(193,46,12,1)",
          data: [{
            x: 120,
            y: 18500,
            r: 15
          }]
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Top 5 sales'
      }, scales: {
        yAxes: [{ 
          scaleLabel: {
            display: true,
            labelString: "Rs"
          }
        }],
        xAxes: [{ 
          scaleLabel: {
            display: true,
            labelString: "Kgs"
          }
        }]
      }
    }
    });
  }
  
  createDoughnutChart(){
    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
      type: "doughnut",
      data: {
        datasets: [
          {
            label: ["Fresh Items", "Groceries", "Eatables", "Personal Supplies", "House Hold"],
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)"
            ],
            hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56"]
          }
        ]
      },
      options: {
        title: {
          display: true,
          text: 'Catogery wise sales'
        }
      }
    });
  }

  createHorizontalBarChart(){
    this.hBarChart = new Chart(this.hBarCanvas.nativeElement,{
      type: 'horizontalBar',
    data: {
      labels: ["8-9AM", "9-10AM", "4-5PM", "6-7PM", "8-9PM"],
      datasets: [
        {
          label: "Sales (in INR)",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: [2478,5267,734,784,433]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Top 5 sales Hours'
      }
    }
    });
  }

  createGroupedBarChart(){
    this.gBarChart = new Chart(this.gBarCanvas.nativeElement,{
      type: 'bar',
    data: {
      labels: ["1900", "1950", "1999", "2050"],
      datasets: [
        {
          label: "Rice",
          backgroundColor: "#3e95cd",
          data: [133,221,783,2478]
        }, {
          label: "Sugar",
          backgroundColor: "#8e5ea2",
          data: [408,547,675,734]
        },
        {
          label: "Wheat",
          backgroundColor: "#8e5ea2",
          data: [408,547,675,734]
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Top 3 Items In Peak Hour Sales'
      }
    }
    });
  }
}
