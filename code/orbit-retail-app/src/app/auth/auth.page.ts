import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { LoadingController } from '@ionic/angular';

import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss']
})
export class AuthPage implements OnInit {
  isLoading = false;
  isLogin = true;
  public email : string = "ganeshjega@gmail.com";
  public password : string = "jegadheesan";

  constructor(
    private authService: AuthService,
    private router: Router,
    private loadingCtrl: LoadingController,
    public httpClient: HttpClient
  ) {}

  ngOnInit() {
  }

  onLogin() {
    this.isLoading = true;
    // this.authService.login('x','y');
    this.loadingCtrl
      .create({ keyboardClose: true, message: 'Logging in...' })
      .then(loadingEl => {
        loadingEl.present();
        // setTimeout(() => {
        //   if (this.authService.userIsAuthenticated) {
        //     this.isLoading = false;
        //     loadingEl.dismiss();
        //     this.router.navigateByUrl('/places/tabs/discover');
        //     }S
        // }, 1500);
      });
  }

  onSwitchAuthMode() {
    this.isLogin = !this.isLogin;
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    this.email = form.value.email;
    this.password = form.value.password;
    console.log(this.email, this.password);

    if (this.isLogin) {
      console.log("[onSubmit] login");
      this.sendPostRequest(this.email, this.password);
      // Send a request to login servers
    } else {
      // Send a request to signup servers
    }
  }

  sendPostRequest(username, password) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept' : 'application/json',
        'Content-Type':  'application/json'
        })
      };

    let postData = {
            "username": username,
            "password": password
    }

    this.httpClient.post("http://localhost:8240/OrionAPIServices/GenericTransactionService/login", postData, httpOptions)
      .subscribe(data => {
        console.log(data);
        this.isLoading = false;
        setTimeout(() => {
          if (this.loadingCtrl) {
            console.log("Gonna dismiss the loading ctrl.");
            this.loadingCtrl.dismiss();
          } else {
            console.log("invalid loading ctrl");
          }
        }, 1500);
        this.router.navigateByUrl('/places/tabs/discover');
       }, error => {
        console.log(error);
      });
  }  
}
