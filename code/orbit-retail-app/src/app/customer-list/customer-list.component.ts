import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { CustomerListService } from './customer-list.service';

export interface Data {
  movies: string;
}

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CustomerListComponent implements OnInit {

  public data: Data;
  public columns: any;
  public rows: any;

  constructor(
    private http: HttpClient,
    private router: Router,
    private customerListService: CustomerListService
  ) {
    this.columns = [
      { name: 'Id', prop:'id' },
      { name: 'CustomerId', prop: 'CustomerId' },
      { name: 'CustomerCode', prop: 'CustomerCode' },
      { name: 'Name', prop: 'Name' },
      { name: 'Mobile', prop: 'Mobile' }
    ];

    this.customerListService.getAllCustomers(); //.subscribe(data => {
      setTimeout(
        () => {
          console.log("Hi...", customerListService.customerList);
          this.rows = customerListService.customerList;
        }, 1500);
      
/*      
      }, error => {
      console.log(error);
    });
*/    
    
/*
    this.http.get<Data>('../../assets/movies.json')
      .subscribe((res) => {
        console.log(res)
        this.rows = res.movies;
      });
*/      
  }

  onActivate(event: any) {
    if(event.type == 'click') {
        console.log(event.row);
        console.log(event.row.id);
        if(event.row.id) {
          this.router.navigateByUrl('/customer-detail');
        }
    }
  }

  ngOnInit() {
    console.log("CustomerListComponent ngonInit");
  }

}
