import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CustomerListComponent } from './customer-list.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {CustomerDetailComponent} from './customer-detail/customer-detail.component';

const routes: Routes = [
  {
    path: '',
    component: CustomerListComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    NgxDatatableModule
  ],
  declarations: [CustomerListComponent, CustomerDetailComponent]
})
export class CustomerListModule {}